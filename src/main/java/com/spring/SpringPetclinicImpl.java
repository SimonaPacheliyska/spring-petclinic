package com.spring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringPetclinicImpl {

	public static void main(String[] args) {
		SpringApplication.run(SpringPetclinicImpl.class, args);
	}

}
